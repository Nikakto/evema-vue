FROM node:9.11.1-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.13.12-alpine as production-stage
COPY --from=build-stage /app/dist /etc/nginx/html
COPY --from=build-stage /app/config/evema.conf /etc/nginx/conf.d/evema.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]