import axios from 'axios';

let evema_api = process.env.EVEMA_API


export function api_get_item_market_menu() {

    let args = {
        ignoreVueAuth: true,
        timeout: 30000
    }

    let url = `${evema_api}/market/menu/`
    return axios.get(url, args)

}


export function api_get_market_orders(item_id, page, is_buy_order, field, desc, filters) {

    let args = {
        params: {
            item_type: item_id,
            is_buy_order: is_buy_order,
            ordering: `${desc ? '-' : ''}${field}`,
            page: page,
            region: filters ? filters.region : null,
            station: filters ? filters.station : null,
            system: filters ? filters.system : null,
        }
    }

    if (field == 'station') {
        args.params.ordering = desc ? '-region,-system,-station' : 'region,system,station'
    }

    let url = `${evema_api}/market/orders/`
    return axios.get(url, args)

}
