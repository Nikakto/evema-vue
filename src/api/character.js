import axios from 'axios'

let evema_api = process.env.EVEMA_API


export function api_get_character_orders(item_id, page, is_buy_order, field, desc, filters) {

    let args = {
        params: {
            item_type: item_id,
            is_buy_order: is_buy_order,
            ordering: `${desc ? '-' : ''}${field}`,
            page: page,
            region: filters ? filters.region : null,
            station: filters ? filters.station : null,
            system: filters ? filters.system : null,
        }
    }

    if (field == 'station') {
        args.params.ordering = desc ? '-region,-system,-station' : 'region,system,station'
    }

    let url = `${evema_api}/character/orders/`
    return axios.get(url, args)

}


export function api_get_character_scopes_grouped() {

    let url = `${evema_api}/character/scopes/grouped/`
    return axios.get(url, {ignoreVueAuth: true})

}


export function api_character_missed_scopes(user, scopes) {
    let scopes_to_check = typeof(scopes) === 'string' ? [scopes] : scopes
    return scopes_to_check.filter(scope => {return !user.scopes || !user.scopes.includes(scope)})   
}