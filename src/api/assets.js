import axios from 'axios'

let evema_api = process.env.EVEMA_API


export function api_get_assets_item_types(search) {

    let source = axios.CancelToken.source()
    let args = {

        cancelToken: source.token,
        ignoreVueAuth: true,       
        params: {
            search: search
        },
    }

    let url = `${evema_api}/assets/items/`
    return [axios.get(url, args), source]

}


export function api_get_assets_item_type(item_type_id) {

    let source = axios.CancelToken.source()
    let args = {
        cancelToken: source.token,
        ignoreVueAuth: true,
    }

    let url = `${evema_api}/assets/items/${item_type_id}/`
    return [axios.get(url, args), source]

}