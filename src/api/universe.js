import axios from 'axios'

let evema_api = process.env.EVEMA_API


export function api_get_universe_get_regions(search) {

    let source = axios.CancelToken.source()
    let args = {
        cancelToken: source.token,
        ignoreVueAuth: true,
        params: {
            search: search
        },
    }

    let url = `${evema_api}/universe/regions/`
    return [axios.get(url, args), source]

}


export function api_get_universe_get_stations(search) {

    let source = axios.CancelToken.source()
    let args = {
        cancelToken: source.token,
        ignoreVueAuth: true,
        params: {
            search: search
        },
    }

    let url = `${evema_api}/universe/stations/`
    return [axios.get(url, args), source]

}


export function api_get_universe_get_systems(search) {

    let source = axios.CancelToken.source()
    let args = {
        cancelToken: source.token,
        ignoreVueAuth: true,
        params: {
            search: search
        },
    }

    let url = `${evema_api}/universe/systems/`
    return [axios.get(url, args), source]

}
