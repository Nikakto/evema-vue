import axios from 'axios'
import Vue from 'vue'

let evema_api = process.env.EVEMA_API


export function api_get_user_auth_link() {

    let url = `${evema_api}/auth/link/`
    return axios.get(url, {ignoreVueAuth: true})

}
