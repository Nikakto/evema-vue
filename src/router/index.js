import Vue from 'vue'
import Router from 'vue-router'

import IndexPage from '@/components/Index'
import Login from '@/components/pages/Login'
import MarketOrders from '@/components/pages/orders/MarketOrders'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        // {
        //     path: '/',
        //     name: 'index',
        //     component: IndexPage
        // },
        {
            path: '/',
            name: 'market-orders',
            component: MarketOrders,
        },

        {
            path: '/login/',
            name: 'login',
            component: Login,
            meta: { clear: true }
        },

        {
            path: '*',
            redirect: '/',
        }
    ]
})
