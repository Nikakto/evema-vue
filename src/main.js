// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'


import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

Raven
    .config(`${process.env.SENTRY}`)
    .addPlugin(RavenVue, Vue)
    .install();


import VueAnalytics from 'vue-analytics'
Vue.use(VueAnalytics, {
    id: process.env.GOOGLE_ANALYTICS,
    router: router,
})


import VueCookies from 'vue-cookies'
Vue.use(VueCookies)


import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
Vue.use(VueMoment, {
    moment,
})


import VueMq from 'vue-mq'
Vue.use(VueMq, {
    breakpoints: {
      mobile: 1000,
      desktop: Infinity,
    }
  })


import VueScroll from 'vuescroll'
Vue.use(VueScroll)


import axios from 'axios'
axios.defaults.timeout = 15000


import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


Vue.router = router
import VueAuth from '@websanova/vue-auth'

Vue.use(VueAuth, {

    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),

    fetchData: {url: `${process.env.EVEMA_API}/auth/info/`, method: 'GET', enabled: true},
    loginData: {url: `${process.env.EVEMA_API}/auth/login/`, method: 'POST', redirect: false, fetchUser: true},
    logoutData: {url: `${process.env.EVEMA_API}/auth/logout/`, method: 'POST', redirect: '/', makeRequest: true},
    refreshData: {url: `${process.env.EVEMA_API}/auth/info/`, method: 'GET', enabled: true, interval: 30},

    tokenStore: ['cookie'],

});

import Devices from '@/mixins/Devices.vue';
Vue.mixin(Devices); 

import VueScrollMixin from '@/mixins/VueScroll.vue';
Vue.mixin(VueScrollMixin); 

import { mixin as clickaway } from 'vue-clickaway';
Vue.mixin(clickaway); 

import '@/styles/animations.scss'
import '@/styles/elements.scss'
// import '@/styles/fonts.scss'
import '@/styles/tabs.scss'
import '@/styles/variables.scss'


Vue.config.productionTip = false

new Vue({
    el: '#app',
    components: { App },
    router: router,
    template: '<App/>'
})
