'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',

  EVEMA_API: '"http://192.168.0.42:8000"',
  GOOGLE_ANALYTICS: '"UA-126257930-2"',
  SENTRY: "''",

})
