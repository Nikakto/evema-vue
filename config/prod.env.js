'use strict'
module.exports = {
  
  NODE_ENV: '"production"',

  EVEMA_API: '"/api"',
  GOOGLE_ANALYTICS: '"UA-126257930-1"',
  SENTRY: "'https://840c69223f424e32ac6323b7dd82df26@sentry.io/1282599'",

}
